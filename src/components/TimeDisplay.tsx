import * as React from "react";
import { useSelector } from "react-redux";
import { RootState, WatchState } from "../store/root";

export const TimeDisplay: React.FC = () => {
  const display = useSelector((state: RootState) => state.display);
  const watchState = useSelector((state: RootState) => state.watchState);

  return (
    <div
      className={`time-display ${
        watchState === WatchState.FINISHED ? "finished" : ""
      }`}
    >
      <code>{display}</code>
    </div>
  );
};
