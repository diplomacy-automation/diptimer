import { Button } from "@material-ui/core";
import { Pause, PlayArrow } from "@material-ui/icons";
import { create } from "fp-ts/Date";
import { IO } from "fp-ts/IO";
import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import { noSleep, runHandler, speak } from "../store/io";
import {
  getTimerEntry,
  IReset,
  IStopWatchPause,
  IStopWatchResume,
  IStopWatchStart,
  IStopWatchTick,
  RESET,
  RootState,
  STOP_WATCH_PAUSE,
  STOP_WATCH_RESUME,
  STOP_WATCH_START,
  STOP_WATCH_TICK,
  WatchState,
} from "../store/root";

const doStopWatchPause = (now: IO<Date>): IStopWatchPause => ({
  type: STOP_WATCH_PAUSE,
  payload: {
    now: now(),
  },
});

const doStopWatchStart = (
  duration: number,
  startedAt: Date,
  timeoutId: number
): IStopWatchStart => ({
  type: STOP_WATCH_START,
  payload: {
    duration,
    startedAt,
    timeoutId,
  },
});
const doStopWatchResume = (
  startedAt: Date,
  timeoutId: number
): IStopWatchResume => ({
  type: STOP_WATCH_RESUME,
  payload: {
    startedAt,
    timeoutId,
  },
});
const doReset = (): IReset => ({
  type: RESET,
});

export const ControlButtonsComponent: React.FC = () => {
  const watchState = useSelector((state: RootState) => state.watchState);
  const rootState = useSelector((state: RootState) => state);
  const dispatch = useDispatch();
  const stopWatchTick = React.useCallback(
    (tick: () => void) => {
      const ret: IStopWatchTick = {
        type: STOP_WATCH_TICK,
        payload: {
          tick,
        },
      };
      dispatch(ret);
    },
    [dispatch]
  );
  const stopWatchGo = React.useCallback(
    async (startedAt: IO<Date>, tick: () => void) => {
      const { menuIndex, timerIndex } = rootState;
      await noSleep.enable();
      if (watchState === WatchState.STANDBY) {
        const entry = getTimerEntry(menuIndex, timerIndex);
        speak(`${entry.title}です`);
        return dispatch(
          doStopWatchStart(entry.duration, startedAt(), runHandler(tick))
        );
      }
      return dispatch(doStopWatchResume(startedAt(), runHandler(tick)));
    },
    [dispatch, rootState, watchState]
  );
  const stopWatchPause = React.useCallback(() => {
    noSleep.disable();
    return dispatch(doStopWatchPause(create));
  }, [dispatch]);

  const onPlayClick = React.useCallback(async () => {
    const tick0 = () => stopWatchTick(tick0);
    if (watchState === WatchState.RUNNING) {
      stopWatchPause();
    } else {
      await stopWatchGo(create, tick0);
    }
  }, [stopWatchGo, stopWatchPause, stopWatchTick, watchState]);

  const onResetClick = React.useCallback(() => {
    noSleep.disable();
    return dispatch(doReset());
  }, [dispatch]);

  return (
    <div className="control-buttons">
      <Button
        variant="contained"
        className="button"
        onClick={onPlayClick}
        disabled={watchState === WatchState.FINISHED}
      >
        {watchState === WatchState.RUNNING ? <Pause /> : <PlayArrow />}
        {watchState === WatchState.RUNNING ? "Pause" : "Go"}
      </Button>
      <Button
        variant="contained"
        className="button"
        color="secondary"
        onClick={onResetClick}
      >
        Reset
      </Button>
    </div>
  );
};
