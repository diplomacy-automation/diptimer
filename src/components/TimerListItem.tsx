import {
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from "@material-ui/core";
import { Timer, TimerOff } from "@material-ui/icons";
import { lookup, mapWithIndex } from "fp-ts/Array";
import { constant, flow } from "fp-ts/function";
import { getOrElse, map } from "fp-ts/Option";
import printf from "printf";
import * as React from "react";
import { useSelector } from "react-redux";
import { defaultMenuList, IMenuEntry, ITimerEntry } from "../models/menu";
import { RootState } from "../store/root";

export const TimerListItem: React.FC = () => {
  const menuIndex = useSelector((state: RootState) => state.menuIndex);
  const timerIndex = useSelector((state: RootState) => state.timerIndex);
  return flow(
    map((a: IMenuEntry) => (
      <List>
        {mapWithIndex((i: number, e: ITimerEntry) => (
          <ListItem
            button={true}
            disabled={true}
            className={i === timerIndex ? "timer-list-selected" : "timer-list"}
            key={i}
          >
            <ListItemText>
              {printf(
                "%s %d:%02d",
                e.title,
                Math.floor(e.duration / 60),
                e.duration % 60
              )}
            </ListItemText>
            <ListItemSecondaryAction>
              <IconButton aria-label="Delete">
                {i === timerIndex ? <Timer /> : <TimerOff />}
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))(a.timers)}
      </List>
    )),
    getOrElse(constant(<React.Fragment />))
  )(lookup(menuIndex, defaultMenuList.entries));
};
