import { Divider, List } from "@material-ui/core";
import * as React from "react";
import { ControlButtonsComponent } from "./ControlButtons";
import { SelectMenu } from "./SelectMenu";
import { TimeDisplay } from "./TimeDisplay";
import { TimerListItem } from "./TimerListItem";

export const GameTimerApp: React.FC = () => {
  return (
    <div>
      <SelectMenu />
      <TimerListItem />
      <List>
        <Divider />
        <TimeDisplay />
        <ControlButtonsComponent />
      </List>
    </div>
  );
};
