import { MenuItem, Select } from "@material-ui/core";
import { map, mapWithIndex } from "fp-ts/Array";
import { flow } from "fp-ts/function";
import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import { defaultMenuList, IMenuEntry } from "../models/menu";
import { ISetMenuIndex, RootState, SET_MENU_INDEX } from "../store/root";

const doSetMenuIndex = (menuIndex: number): ISetMenuIndex => ({
  type: SET_MENU_INDEX,
  payload: { menuIndex },
});

export const SelectMenu: React.FC = () => {
  const menuIndex = useSelector((state: RootState) => state.menuIndex);
  const dispatch = useDispatch();
  const onMenuSelect = React.useCallback(
    (ev: React.ChangeEvent<{ name?: string; value: unknown }>) => {
      dispatch(doSetMenuIndex(parseInt(ev.target.value as string, 10)));
    },
    [dispatch]
  );

  return (
    <Select value={menuIndex} onChange={onMenuSelect}>
      {flow(
        map((e: IMenuEntry) => e.name),
        mapWithIndex((i: number, n: string) => (
          <MenuItem value={i} key={i}>
            {n}
          </MenuItem>
        ))
      )(defaultMenuList.entries)}
    </Select>
  );
};
