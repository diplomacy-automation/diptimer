import { head, lookup, tail } from "fp-ts/Array";
import { create } from "fp-ts/Date";
import { constant, flow } from "fp-ts/function";
import { chain, exists, fold, getOrElse, Option } from "fp-ts/Option";
import { Lens } from "monocle-ts";
import { defaultMenuList, IMenuEntry, ITimerEntry } from "../models/menu";
import {
  displayFormat,
  fromSeconds,
  getCheckPoints,
  isoMilliSecond,
  MilliSecond,
  speechString,
  toSeconds,
} from "../models/time";
import { noSleep, runHandler, speak } from "./io";

export const SET_MENU_INDEX = "SET_MENU_INDEX";
export interface ISetMenuIndex {
  type: typeof SET_MENU_INDEX;
  payload: {
    menuIndex: number;
  };
}

export const STOP_WATCH_PAUSE = "STOP_WATCH_PAUSE";
export interface IStopWatchPause {
  type: typeof STOP_WATCH_PAUSE;
  payload: {
    now: Date;
  };
}

export const STOP_WATCH_START = "STOP_WATCH_START";
export interface IStopWatchStart {
  type: typeof STOP_WATCH_START;
  payload: {
    duration: number;
    startedAt: Date;
    timeoutId: number;
  };
}

export const STOP_WATCH_RESUME = "STOP_WATCH_RESUME";
export interface IStopWatchResume {
  type: typeof STOP_WATCH_RESUME;
  payload: {
    startedAt: Date;
    timeoutId: number;
  };
}

export const RESET = "RESET";
export interface IReset {
  type: typeof RESET;
}

export const STOP_WATCH_TICK = "STOP_WATCH_TICK";
export interface IStopWatchTick {
  type: typeof STOP_WATCH_TICK;
  payload: {
    tick: () => void;
  };
}
export type RootAction =
  | ISetMenuIndex
  | IStopWatchPause
  | IStopWatchStart
  | IStopWatchResume
  | IReset
  | IStopWatchTick;

export enum WatchState {
  STANDBY,
  RUNNING,
  SUSPEND,
  FINISHED,
}
interface IRootState {
  menuIndex: number;
  timerIndex: number;
  display: string;
  watchState: WatchState;
  left: MilliSecond;
  checkPoints: number[];
  startedAt: Date;
}
const menuIndex = Lens.fromProp<IRootState>()("menuIndex");
const timerIndex = Lens.fromProp<IRootState>()("timerIndex");
const display = Lens.fromProp<IRootState>()("display");
const watchState = Lens.fromProp<IRootState>()("watchState");
const left = Lens.fromProp<IRootState>()("left");
const checkPoints = Lens.fromProp<IRootState>()("checkPoints");
const startedAt = Lens.fromProp<IRootState>()("startedAt");

export type RootState = IRootState;

export const defaultRootState: RootState = {
  menuIndex: 0,
  timerIndex: 0,
  display: "",
  watchState: WatchState.STANDBY,
  left: isoMilliSecond.wrap(0),
  checkPoints: [],
  startedAt: new Date(),
};

export const rootReducer = (
  state: IRootState = reset(defaultRootState),
  action: RootAction
): IRootState => {
  switch (action.type) {
    case STOP_WATCH_START:
      return flow(
        watchState.set(WatchState.RUNNING),

        left.set(fromSeconds(action.payload.duration)),
        checkPoints.set(getCheckPoints(action.payload.duration)),

        startedAt.set(action.payload.startedAt)
      )(state);

    case STOP_WATCH_RESUME:
      return flow(
        watchState.set(WatchState.RUNNING),

        startedAt.set(action.payload.startedAt)
      )(state);
    case STOP_WATCH_PAUSE:
      return flow(
        left.set(getLeftMilli(action.payload.now)(state)),
        watchState.set(WatchState.SUSPEND)
      )(state);
    case SET_MENU_INDEX:
      return flow(menuIndex.set(action.payload.menuIndex), reset)(state);
    case RESET:
      return reset(state);
    case STOP_WATCH_TICK:
      const tick: () => void = action.payload.tick;
      if (state.watchState !== WatchState.RUNNING) {
        return state;
      }
      const remainMilliSecond = getLeftMilli(new Date())(state);

      if (isoMilliSecond.unwrap(remainMilliSecond) <= 0) {
        return fold(
          () => {
            noSleep.disable();
            speak("終了です。");
            // return dispatch(doFinish());
            return flow(
              display.set(displayFormat(isoMilliSecond.wrap(0))),
              watchState.set(WatchState.FINISHED)
            )(state);
          },
          (e: ITimerEntry) => {
            speak(`${e.title}です`);
            runHandler(tick);
            // return dispatch(
            //   doNextTimer(runHandler(tick), e.duration, new Date())
            // );
            return flow(
              timerIndex.modify((n) => n + 1),

              left.set(fromSeconds(e.duration)),
              checkPoints.set(getCheckPoints(e.duration)),

              startedAt.set(new Date())
            )(state);
          }
        )(getNextTimer(state));
      }

      const hit = flow(
        exists((second: number) => toSeconds(remainMilliSecond) < second)
      )(head(state.checkPoints));
      if (hit) {
        speak(speechString(remainMilliSecond));
      }
      // return dispatch(doRunTick(runHandler(tick), hit, create));
      runHandler(tick);
      return flow(
        display.set(displayFormat(getLeftMilli(create())(state))),
        checkPoints.modify((cs) =>
          hit ? flow(getOrElse(constant<number[]>([])))(tail(cs)) : cs
        )
      )(state);
    default:
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const _: never = action;
      return state;
  }
};

export const getTimerEntry = (menu: number, timer: number): ITimerEntry => {
  return flow(
    chain((a: IMenuEntry) => lookup(timer, a.timers)),
    getOrElse(constant({ title: "", duration: 0 }))
  )(lookup(menu, defaultMenuList.entries));
};

export const getNextTimer = (self: RootState): Option<ITimerEntry> =>
  flow(chain((a: IMenuEntry) => lookup(self.timerIndex + 1, a.timers)))(
    lookup(self.menuIndex, defaultMenuList.entries)
  );

export const getLeftMilli =
  (now: Date) =>
  (state: RootState): MilliSecond => {
    return isoMilliSecond.wrap(
      isoMilliSecond.unwrap(state.left) -
        (now.getTime() - state.startedAt.getTime())
    );
  };

export const reset = (state: RootState): RootState => {
  const duration = getTimerEntry(state.menuIndex, 0).duration;
  return flow(
    timerIndex.set(0),
    watchState.set(WatchState.STANDBY),
    display.set(flow(fromSeconds, displayFormat)(duration))
  )(state);
};
