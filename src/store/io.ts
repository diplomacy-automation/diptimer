import NoSleep from "nosleep.js";

export const noSleep = new NoSleep();

export const runHandler = (handler: () => void): number => {
  return window.setTimeout(handler, 100);
};

export const speak = (text: string): void => {
  const synthes = new SpeechSynthesisUtterance(text);
  synthes.lang = "ja-JP";
  synthes.rate = 1.0;
  speechSynthesis.speak(synthes);
};
