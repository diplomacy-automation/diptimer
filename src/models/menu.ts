import { getMonoid, map, range } from "fp-ts/Array";
import { concatAll } from "fp-ts/Monoid";

export interface IMenuList {
  entries: IMenuEntry[];
}

export interface IMenuEntry {
  name: string;
  timers: ITimerEntry[];
}

export interface ITimerEntry {
  title: string;
  duration: number;
}

export const defaultMenuList: IMenuList = {
  entries: concatAll(getMonoid<IMenuEntry>())([
    [
      {
        name: "ディプロマシー",
        timers: [
          { title: "外交フェイズ", duration: 15 * 60 },
          { title: "命令記述フェイズ", duration: 5 * 60 },
          { title: "命令解決フェイズ", duration: 10 * 60 },
        ],
      },
      {
        name: "テスト",
        timers: [
          { title: "A", duration: 2 },
          { title: "B", duration: 4 },
          { title: "C", duration: 3 },
        ],
      },
      {
        name: "テスト2",
        timers: [{ title: "A", duration: 1 }],
      },
    ],
    map((i: number) => ({
      name: `${i}分`,
      timers: [{ title: `${i}分`, duration: i * 60 }],
    }))(range(1, 15)),
  ]),
};

export const defaultTimerEntry: ITimerEntry = {
  title: "",
  duration: 0,
};
