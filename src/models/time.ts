import { filter, getMonoid, map, range, reverse } from "fp-ts/Array";
import { flow } from "fp-ts/function";
import { concatAll } from "fp-ts/Monoid";
import { iso, Newtype } from "newtype-ts";
import printf from "printf";

interface IMilliSecond
  extends Newtype<{ readonly MilliSecond: unique symbol }, number> {}
export type MilliSecond = IMilliSecond;

export const isoMilliSecond = iso<MilliSecond>();

export const toSeconds = (r: MilliSecond): number =>
  isoMilliSecond.unwrap(r) / 1000;
export const fromSeconds = (s: number): MilliSecond =>
  isoMilliSecond.wrap(s * 1000);

export const displayFormat = (r: MilliSecond): string => {
  const sec = Math.ceil(isoMilliSecond.unwrap(r) / 1000);
  return sec <= 0
    ? "00:00:00"
    : printf(
        "%02d:%02d:%02d",
        sec / 3600,
        Math.floor(sec % 3600) / 60,
        Math.floor(sec) % 60
      );
};
export const speechString = (r: MilliSecond): string => {
  const left = Math.ceil(toSeconds(r));
  if (left < 10) {
    return left.toString();
  }

  const hour = Math.floor(left / 3600);
  const min = Math.floor((left % 3600) / 60);
  const sec = Math.floor(left % 60);
  return `残り${hour > 0 ? `{hour}時間` : ""}
        ${min > 0 ? `${min}分` : ""}
        ${sec > 0 ? `${sec}秒` : ""}
        です`;
};

export const getCheckPoints = (limit: number): number[] => {
  return flow(
    concatAll(getMonoid<number>()),
    filter((e: number) => e < limit),
    reverse
  )([
    range(1, 5),
    map((i: number) => i * 10)(range(1, 5)),
    map((i: number) => i * 60)(range(1, 15)),
  ]);
};
